<?php

class Nextcommerce_Articles_Block_Articles extends Mage_Core_Block_Template
{

    public function getArticlesCollection()
    {
        $articlesCollection = Mage::getModel('nart/articles')->getCollection();
		return $articlesCollection;
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

}