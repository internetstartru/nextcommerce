<?php

class Nextcommerce_Articles_Block_Adminhtml_Articles_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'nart';
        $this->_controller = 'adminhtml_articles';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('nart');
        $model = Mage::registry('current_articles');

        if ($model->getId()) {
            return $helper->__("Edit Article item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add Article item");
        }
    }

	protected function _prepareLayout() {
		parent::_prepareLayout();
		
		$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		
	}
}