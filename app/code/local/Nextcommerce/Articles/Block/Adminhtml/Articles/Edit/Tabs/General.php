<?php

class Nextcommerce_Articles_Block_Adminhtml_Articles_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        $helper = Mage::helper('nart');
        $model = Mage::registry('current_articles');


        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array('legend' => $helper->__('General Information')));

        $fieldset->addField('title', 'text', array(
            'label' => $helper->__('Title'),
            'required' => true,
            'name' => 'title',
        ));
        
		$fieldset->addField('description', 'editor', array(
	'name'      => 'description',
	'label'     => $helper->__('Description'),
	'title'     => $helper->__('Description'),
	'wysiwyg'   => true,
	'required'  => false,
));
	/*
		$fieldset->addField('description', 'editor', array(
	'name'      => 'description',
	'label'     => $helper->__('Content'),
	'title'     => $helper->__('Content'),
	'style'     => 'height:15em',
	'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
	'wysiwyg'   => true,
	'required'  => false,
));
	*/	

        $fieldset->addField('image', 'image', array(
            'label' => $helper->__('Image'),
            'name' => 'image',
        ));

		$fieldset->addField('status', 'select', array(
            'name'     => 'status',
            'title'    => 'Status',
            'label'    => 'Status',
            'required' => true,
            'values'   => array(
				0=>array('value'=>'1', 'label'=>'Enable'),
				1=>array('value'=>'0', 'label'=>'Disable'),
			)
        ));
		
		
		

        $formData = array_merge($model->getData(), array('image' => $model->getImageUrl()));
        $form->setValues($formData);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
