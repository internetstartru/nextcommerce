<?php

class Nextcommerce_Articles_Block_Adminhtml_Articles extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();
		
        $helper = Mage::helper('nart');
		
        $this->_blockGroup = 'nart';
        $this->_controller = 'adminhtml_articles';

        $this->_headerText = $helper->__('Articles');
        $this->_addButtonLabel = $helper->__('Add Article');
    }

}