<?php

class Nextcommerce_Articles_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $articleId = Mage::app()->getRequest()->getParam('article_id', 0);
        $articles = Mage::getModel('nart/articles')->load($articleId);

        if ($articles->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('articles.content')->assign(array(
                "articlesItem" => $articles,
            ));
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }

}